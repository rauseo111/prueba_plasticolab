<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(is_array($result) && sizeof($result)>0){ ?>
<div class="pagination" style="float:right;"> <?php echo $paginglinks; ?></div>
<div class="pagination" style="float:left;"> <?php echo (!empty($pagermessage) ? $pagermessage : ''); ?></div>
<table width="100%" cellspacing="0" cellpadding="4" border="0" class="data">
  <tbody>
    <tr>
      <th width="7%">Fecha y Hora</th>
      <th width="10%">Peso</th>
      <th width="10%">Altura</th>
      <th width="10%">IMC</th>
      <th width="10%">Clasificacion</th>
    </tr>
    <?php foreach($result as $key=>$v) {?>
        <tr class="">
        <td><?php echo $v['fecha_hora']?></td>
        <td><?php echo $v['peso']?></td>
        <td><?php echo $v['altura']?></td>
        <td><?php echo $v['imc_calculado']?></td>
	<td><?php echo $v['clasificacion']?></td>	
        <td><?php echo date('d/m/Y H:i:s');?></td>
        <tr>
      <?php }?>
  </tbody>
</table>
<div class="pagination" style="float:right;"> <?php echo $paginglinks; ?></div>
<div class="pagination" style="float:left;"> <?php echo (!empty($pagermessage) ? $pagermessage : ''); ?></div>
<?php }else{?>
<p align="center" style="padding-top:20px;">
  <?php  echo 'No Record Found!' ;?>
</p>
<?php }?>
