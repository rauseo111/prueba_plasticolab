<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prueba</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<!-- css -->
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<header id="site-header">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">

					<?php if (isset($_SESSION['email']) && $_SESSION['logged_in'] === true) : ?>
			

						<a class="navbar-brand" href="<?= base_url('calculoindex') ?>">Calcular IMC</a>
						<a class="navbar-brand" href="<?= base_url('pagdatatable') ?>">Historico</a>
					<?php endif; ?>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<?php if (isset($_SESSION['email']) && $_SESSION['logged_in'] === true) : ?>
				</br>
				<li><p><b>Usuario </b> <?php echo $_SESSION['email'];?> | <a href="<?= base_url('logout') ?>">Cerrar Sesion</a>
</p></li>		
					<li></li>
						<?php else : ?>
							<li><a href="<?= base_url('register') ?>">Registrar Usuario</a></li>
							<li><a href="<?= base_url('login') ?>">Login</a></li>
						<?php endif; ?>
					</ul>
				</div><!-- .navbar-collapse -->
			</div><!-- .container-fluid -->
		</nav><!-- .navbar -->
	</header><!-- #site-header -->
