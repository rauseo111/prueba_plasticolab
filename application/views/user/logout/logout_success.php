<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>La Sesion ha sido Cerrada con Éxito!</h1>
			</div>
			<p>Ahora estás registrado.</p>
		</div>
	</div><!-- .row -->
</div><!-- .container -->
