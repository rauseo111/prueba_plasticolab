<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-md-12">
			<div class="page-header">
				<h1>Crear Usuario</h1>
			</div>
			<?php echo form_open() ?>
				<div class="form-group">
					<label for="username">Nombre</label>
					<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese Nombre">
					
				</div>
				<div class="form-group">
					<label for="username">Apellido</label>
					<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingrese Apellido">
					
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Ingrese Email">
					
				</div>
				<div class="form-group">
					<label for="password">Clave</label>
					<input type="password" class="form-control" id="clave" name="clave" placeholder="Ingrese Clave">
					
				</div>
				<div class="form-group">
					<label for="password_confirm">Confirmar Clave</label>
					<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm your password">
					
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-default" value="Registrar">
				</div>
			</form>
		</div>
	</div><!-- .row -->
</div><!-- .container -->
