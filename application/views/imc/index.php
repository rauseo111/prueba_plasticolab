<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/script.js') ?>"></script>
<script>
$(document).ready(function() 
{
   // Interceptamos el evento submit
    $('#exito').click(function() 
    {
	var peso= $("#peso").val();
        var altura = $("#altura").val();
	if(peso!="" || altura!="")
	{
				
       			$.ajax({
				type: "POST",
				url: "<?php echo base_url('calculosave'); ?>",
				dataType: 'json',
				data: {peso: peso, altura: altura},
				success: function(res) 
				{
					if(res)
					{
				alert("Se ha Registrado su IMC/Puede Consultarlo Atravez de la Opcion Historico");
				$("#result").html('<p><center><label>Peso:</label>'+res.peso+'</br><label>Altura:'+res.altura+'</label></br><label>IMC:'+res.imc+'</label></br><label>Clasificación:'+res.clasificacion+'</label></center></p>');
						
					}
				}
			});

	}else{alert("Los Campos Peso y Altura son Requeridos/Solo se Permiten Valores Numericos");}
				
     });
});
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Calcular IMC</h1>

			<form id="fo3">							
				<div class="form-group">
					<label for="altura">Altura</label>
					<input type="altura" class="form-control" id="altura" name="altura" placeholder="Ingrese Altura (Cm)">*
					
				</div>
				<div class="form-group">
					<label for="peso">Peso</label>
					<input type="text" class="form-control" id="peso" name="peso" placeholder="Ingrese Peso en (Kilogramos)">*			
				</div>	
				<div class="form-group">
					<input type="button" class="btn btn-default" id="exito" value="Calcular">
				
				
					<input type="reset" class="btn btn-default" id="reset" value="Limpiar">
				</div>
			</form>
			<div id="result"></div>
		</div>
			</div>
		</div>
	</div><!-- .row -->
</div><!-- .container -->

