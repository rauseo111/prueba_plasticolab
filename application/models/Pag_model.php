<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pag_model extends CI_Model {


	public function __construct() 
	{
		
		parent::__construct();
		$this->load->database();
		
	}

   public function total_count() 
		{
       		return $this->db->count_all("cal_imc");
    		}

    public function get_users($limit, $start) 
	{
	      $this->db->limit($limit, $start);
	      $query = $this->db->get("cal_imc");
	      if ($query->num_rows() > 0) 
		{
		return $query->result_array();
		}
      return false;
       }
	
}
