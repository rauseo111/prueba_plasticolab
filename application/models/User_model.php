<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class User_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($nombre,$apellido, $email, $clave) {
		
		$data = array(
			'nombre'=> $nombre,
			'apellido'=> $apellido,
			'email' => $email,
			'clave'   => $this->hash_password($clave)
		);
		
		return $this->db->insert('users', $data);
		
	}
	
	/**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($email, $clave) {
		
		$this->db->select('clave');
		$this->db->from('users');
		$this->db->where('email', $email);
		$hash = $this->db->get()->row('clave');
		
		return $this->verify_password_hash($clave, $hash);
		
	}

	public function getemail($email) {
		
		$this->db->select('email');
		$this->db->from('users');
		$this->db->where('email', $email);
		return $this->db->get()->row('email');
		
		
		
	}
	
	public function getclave($clave) {
		
		$this->db->select('clave');
		$this->db->from('users');
		$this->db->where('clave', $clave);
		return $this->db->get()->row('clave');
		
		
		
	}
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_user_id_from_username($email) {
		
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('email', $email);

		return $this->db->get()->row('id');
		
	}
	
	/**
	 * get_user function.
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id) {
		
		$this->db->from('users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
		
	}
	
	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($clave) {
		
		return password_hash($clave, PASSWORD_BCRYPT);
		
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($clave, $hash) {
		
		return password_verify($clave, $hash);
		
	}
	
}
