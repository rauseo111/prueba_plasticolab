<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imc_model extends CI_Model {


	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}

	public function create_imc($fecha,$peso,$altura,$subst,$clasificacion,$usuario) {
		
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('email',$usuario);
		$idusuario=$this->db->get()->row('id');

		
		$data = array(
			'fecha_hora'=> $fecha,
			'peso'=> $peso,
			'altura'=> $altura,
			'imc_calculado'=> $subst,
			'clasificacion'=> $clasificacion,
			'idusuario'=>$idusuario);
				

		return $this->db->insert('cal_imc', $data);
		
	}
	
}
