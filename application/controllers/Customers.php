<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Customers extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
	$this->load->library(array('session'));
        $this->load->model('customers_model','customers');
    }
 
    public function index()
    {
        $this->load->helper('url');
	$this->load->view('header');				
        $this->load->view('customers_view');
	$this->load->view('footer');
    }
 
    public function ajax_list()
    {
	
        $list = $this->customers->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->fecha_hora;
            $row[] = $customers->peso;
            $row[] = $customers->altura;
            $row[] = $customers->imc_calculado;
            $row[] = $customers->clasificacion;
 	    $row[] = $customers->email;
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->customers->count_all(),
                        "recordsFiltered" => $this->customers->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
 
}
