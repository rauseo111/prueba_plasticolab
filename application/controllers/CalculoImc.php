<?php defined('BASEPATH') OR exit('No direct script access allowed');
class CalculoImc extends CI_Controller 
{

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('imc_model');
		
	}
	
	
	public function index() 
		{				
				$this->load->view('header');
				$this->load->view('imc/index');
				$this->load->view('footer');	
		}

	public function calcularimc()
		{
			$peso=(float)$this->input->post('peso');
			$altura=(float)$this->input->post('altura');
		        $estatura=pow($altura,2)/10000;			
			$imc=($peso/$estatura);
			$subst=substr($imc, 0,5);    

			$convert=$subst;

			
			if($convert<18)
			{

				$clasificacion='Bajo peso';
			}else
			if($convert==16)
			{
				$clasificacion='Delgadez severa';
	
			}else
			if($convert >= 16 && $convert<= 16)
			{

				$clasificacion='Delgadez moderada';

			}else
			if($convert >= 17 && $convert <= 18)
			{

				$clasificacion='Delgadez leve';
			}else
			if($convert >= 18 && $convert <= 24)
			{

				$clasificacion='Normal';
			}else
			if($convert >= 25 && $convert<=30)
			{

				$clasificacion='Sobre Peso';
			}else
			if($convert >= 30 && $convert <= 34)
			{

				$clasificacion='Obesidad Leve';
			}else			
			if($convert >= 35 && $convert <= 39)
			{

				$clasificacion='Obesidad Media';
			}else
			if($convert >= 40)
			{

				$clasificacion='Obesidad Mórbida';
			}
	
			$fecha=date("Y-m-d H:i:s");
			$usuario=$_SESSION['email'];
			$data=array(
			'fecha'=>$fecha,
			'peso' => $peso,
			'altura'=> $altura,
			'imc'=>$subst,
			'clasificacion'=>$clasificacion);

			$this->imc_model->create_imc($fecha,$peso,$altura,$subst,$clasificacion,$usuario);
			echo json_encode($data);
			
		}

}
