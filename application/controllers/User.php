<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('user_model');
		
	}
	
	
	public function iniciosesion() {
				
				$this->load->view('header');
				$this->load->view('user/login/login_success');
				$this->load->view('footer');
		
	}
	
	/**
	 * register function.
	 * 
	 * @access public
	 * @return void
	 */
	public function register() {
		
		// create the data object
		$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_message('required', 'El campo %s es obligatorio');
		$this->form_validation->set_message('min_length', 'El Campo %s debe tener un Minimo de %d Caracteres');
		$this->form_validation->set_message('matches', 'El campo Confirmar Clave no coincide con el campo Contraseña.');

		$this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('apellido', 'apellido', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]',array('is_unique' => 'El Email Ingresado Ya Existe/Verifique.'));
		$this->form_validation->set_rules('clave', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('password_confirm', 'Confirme Clave', 'trim|required|min_length[8]|matches[clave]');
		
		if ($this->form_validation->run() === false) {
			
			// validation not ok, send validation errors to the view
			$this->load->view('header');
			$this->load->view('user/register/register', $data);
			$this->load->view('footer');
			
		} else {
			
			// set variables from the form
			$nombre = $this->input->post('nombre');
			$apellido= $this->input->post('apellido');
			$email = $this->input->post('email');
			$clave = $this->input->post('clave');
			
			if ($this->user_model->create_user($nombre,$apellido,$email,$clave)) {
				
				// user creation ok
				$this->load->view('header');
				$this->load->view('user/register/register_success', $data);
				$this->load->view('footer');
				
			} else {
				
				// user creation failed, this should never happen
				$data->error = 'Se Genero un Problema al Generar el Nuevo Usuario/Verifique.';
				
				// send error to the view
				$this->load->view('header');
				$this->load->view('user/register/register', $data);
				$this->load->view('footer');
				
			}
			
		}
		
	}
		
	/**
	 * login function.
	 * 
	 * @access public
	 * @return void
	 */
	public function login() {
		$_SESSION['salida']=(bool)false;
	
		// create the data object
		$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_message('required', 'El campo %s es obligatorio');

		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('clave', 'Clave', 'required');
		
		
		if ($this->form_validation->run() == false) {
			
			
			$this->load->view('header');
			$this->load->view('user/login/login');
			$this->load->view('footer');
			
			
		} else {
			
			// set variables from the form
			$email = $this->input->post('email');
			$clave = $this->input->post('clave');
			
			if ($this->user_model->resolve_user_login($email, $clave)) {
				
				$user_id = $this->user_model->get_user_id_from_username($email);
				
				$user    = $this->user_model->get_user($user_id);
				
				// set session user datas
				$_SESSION['user_id']      = (int)$user->id;
				$_SESSION['email']        = (string)$user->email;
				$_SESSION['logged_in']    = (bool)true;

				
				redirect('iniciosesion');
				
		
				
			
			} else{	
				
				$emailbd =$this->user_model->getemail($this->input->post('email'));	
				$clave=$this->user_model->getclave($this->input->post('clave'));
				if($emailbd!=$this->input->post('email') || $clave!=$this->input->post('clave'))
				{
				$data->error = 'Verifique Email o Clave.';
					// send error to the view	
				$this->load->view('header');
				$this->load->view('user/login/login', $data);
				$this->load->view('footer');		
						
				}elseif(empty($this->input->post('email')) && empty($this->input->post('clave') ))
				{
						
				// send error to the view	
				$this->load->view('header');
				$this->load->view('user/login/login');
				$this->load->view('footer');
				}

				
			}

				

				
			
		}
		
	}
	
	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() {
		
		// create the data object
		$data = new stdClass();
		
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			
			// user logout ok
			//$this->load->view('header');
			//$this->load->view('user/logout/logout_success', $data);
			//$this->load->view('footer');
			redirect('login');
			
		} else {
			
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');
			
		}
		
	}
	
}
